/** @format */

import '../app/styles/style.scss'
// import '../app/styles/fonts.css'
import '../app/styles/hamburger.css'

$(document).ready(function() {
  // toggle the menu icon
  $('#nav-icon1').click(function() {
    console.log('logging')
    $(this).toggleClass('open')
    $('.mobile-nav').toggleClass('show-nav')
  })

  // toggle the modal
  $('.access-btn, .overlay, .close-btn').click(function() {
    $('.my-modal').toggleClass('show-modal')
    $('.wrapper').toggleClass('fixed-height')
  })
})
